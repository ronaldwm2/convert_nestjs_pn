import { Test, TestingModule } from "@nestjs/testing";
import { NotificationController } from "./notification.controller";
import { NotificationService } from "./notification.service";

describe('NotificationService', () => {
    let notificationService: NotificationService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [NotificationService]
        }).compile();
        notificationService = module.get<NotificationService>(NotificationService);
    });

    it('Notification Service Should Be Initialized', () => {
        expect(notificationService).toBeDefined();
    })

    describe('getHello', () => {
        it ('Should be Initialized', () => {
            expect(notificationService.getHello()).toBeDefined();
        })

        it ('Should return Hello World!', () => {
            const getHello = notificationService.getHello();

            expect(getHello).toBe("Hello World!")
        })
    })
});