import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { NotificationService } from './notification.service';

@Controller('notification')
export class NotificationController {
  constructor(private notificationService: NotificationService) {}

  @Get()
  getHello(): string {
    return "hello";
  }

  @Get("getNotificationReportByFilter")
  async getNotificationReportByFilter(@Req() request: any, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.notificationService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }

  @Get("getUserBySegmentation")
  async getUserBySegmentation(@Req() request: any, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.notificationService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }

  @Get("deleteSegmentation")
  async deleteSegmentation(@Req() request: any, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.notificationService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }

  @Post("insertSegmentation")
  async insertSegmentation(@Req() request: any, @Body() body, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.notificationService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }

  @Post("sendFCM")
  async sendFCM(@Req() request: any, @Body() Body, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.notificationService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }
}
