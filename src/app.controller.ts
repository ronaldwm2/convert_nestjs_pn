import { Controller, Get, Req, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get("callEvent")
  async callEvent(@Req() request: any, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.appService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }

  @Get("registerUser")
  async registerUser(@Req() request: any, @Res() res) : Promise<void> {
    try {
        const finalResult: any = this.appService.getHello();
        res.send(finalResult);
    } catch (e) {
        res.status(500);
        res.send({
            message: e.message
        })
    }
  }

  @Get("logMessageReceived")
  async logMessageReceived(@Req() request: any, @Res() res) : Promise<void> {
    try {
      const finalResult: any = this.appService.getHello();
      res.send(finalResult);
  } catch (e) {
      res.status(500);
      res.send({
          message: e.message
      })
  }
  }

  @Get("logMessageOpen")
  async logMessageOpen(@Req() request: any, @Res() res) : Promise<void> {
    try {
      const finalResult: any = this.appService.getHello();
      res.send(finalResult);
  } catch (e) {
      res.status(500);
      res.send({
          message: e.message
      })
  }
  }

  
}
